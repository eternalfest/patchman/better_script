# 4.5.1 (2024-12-29)

- **[Fix]** Add missing dependency to color_matrix.

# 4.5.0 (2023-01-18)

- **[Fix]** Compatibility with merlin 0.17.0.
- **[Feature]** Add `subframe` parameter to `sprite()`.

# 4.4.0 (2022-05-21)

- **[Feature]** Add "flip" parameter. It has an effect on xScale equivalent to applying sx = -1.
- **[Fix]** Correctly calculate position of $torch holes when scaling on the x axis.
- **[Fix]** Correctly calculate the x coordinate when scaling on the x axis.
- **[Fix]** Correctly apply rotation in mirror mode.

# 4.3.0 (2022-05-19)

- **[Feature]** Add sprite() action with more options than mc()

# 4.2.0 (2021-06-12)

- **[Feature]** Accept boolean for NO_NEXT_LEVEL/DEADLY_BOTTOM, deprecate weird inputs
- **[Feature]** Add NO_BOMB script variable
- **[Feature]** Add debug(...) (and DUMP script variable)
- **[Feature]** Add BetterScript file that exports all of better_script

# 4.1.0 (2021-06-08)

- **[Feature]** Add players[id].x/y/lives
- **[Feature]** Add sprites[id].x/y
- **[Feature]** Add rand(n)

# 4.0.2 (2021-05-23)

- **[Fix]** players.spawnX returns the un-mirrored value, suitable for use in assignment to players.x for example.

# 4.0.1 (2021-05-21)

- **[Fix]** players.x/y is now in cells and not in pixels.

# 4.0.0 (2021-05-19)

- **[Feature]** Add players.length/lives/score/spawnX/spawnY/x/y.
- **[Fix]** Update npm registry.

# 3.0.0 (2021-04-21)

- **[Breaking change]** Update to `patchman@0.10.4`.

# 2.0.0 (2021-01-24)

- **[Breaking change]** Refactor library: you can now enable the various features
  by only importing `Bombexpert`, `DeadlyBottom` and `NoNextLevel`.
- **[Internal]** Update to Yarn 2.

# 1.0.0 (2020-12-16)

- **[Feature]** First release.
