# @patchman/better_script

Adds extra features to the game script engine.

#### Usage
```haxe
import hf.Hf;
import patchman.IPatch;
import patchman.Patchman;
import better_script.BetterScript;
import better_script.NoNextLevel;

@:build(patchman.Build.di())
class Main {
  public static function main(): Void {
    Patchman.bootstrap(Main);
  }

  public function new(
    betterScript: BetterScript, // Enable all better_script features
    //noNextLevel: NoNextLevel, // Or enable single better_script feature
    patches: Array<IPatch>,
    hf: Hf
  ) {
    Patchman.patchAll(patches, hf);
  }
}
```

#### Bombexpert

Adds `bombexpert()` action that returns true if bombexpert option is enabled.

#### DeadlyBottom

Adds `DEADLY_BOTTOM` ref to kill players if they reach the bottom of the level.

#### Debug

Adds `debug()` action.

#### NoBomb
Adds `NO_BOMB` ref to disable players attacks.

#### NoNextLevel
Adds `NO_NEXT_LEVEL` ref to disable the next level arrow sprite.

#### Players
Adds `players[id].x/y/lives` to access players properties.

#### Rand
Adds `rand()` action to generate a random integer value.

#### Sprite
Adds `sprite()` action with more options than `mc()`:
- `x / y / xr / yr / sid / back / n / p` options identical to `mc()`
- `sx / sy / s` for scaling
- `flip` equivalent to `sx = -1`
- `rot` for rotation (in degrees)
- `frame` to move the MovieClip to a specific frame
- `sframe` to move the MovieClip to a specific subframe
- `filters` to add a filter (only `filters=filter_hsv(h,s,v)` is supported)
- `alpha` to modify the MovieClip alpha

#### Sprites
Adds `sprites[sid].x/y` to access sprites properties.
