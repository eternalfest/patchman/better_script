package better_script;

import better_script.no_next_level.NoNextLevelRefFactory;
import etwin.Symbol;
import hf.levels.GameMechanics;
import hf.FxManager;
import patchman.PatchList;
import patchman.Ref;
import patchman.IPatch;
import merlin.IRefFactory;

@:build(patchman.Build.di())
class NoNextLevel {
  public static var SYMBOL(default, never): Symbol = new Symbol();

  @:diExport
  public var patch(default, null): IPatch;
  @:diExport
  public var merlinRef(default, null): IRefFactory = new NoNextLevelRefFactory();

  public function new() {
    var patches: Array<IPatch> = [];

    patches.push(Ref.auto(FxManager.attachExit).wrap(function(hf, self: FxManager, old) {
      if (Reflect.field(self.game, SYMBOL.toString())) {
        self.detachExit();
      } else {
        old(self);
      }
    }));
    patches.push(Ref.auto(GameMechanics.onReadComplete).before(function(hf, self: GameMechanics) {
      Reflect.setField(self.game, SYMBOL.toString(), false);
    }));

    this.patch = new PatchList(patches);
  }
}
