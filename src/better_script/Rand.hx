package better_script;

import etwin.Obfu;
import hf.mode.GameMode;
import merlin.ICall;
import merlin.IRef;
import merlin.IRefFactory;
import merlin.ScriptError;
import merlin.refs.FrozenRef;
import merlin.value.MerlinFloat;
import merlin.value.MerlinFunction;
import merlin.value.MerlinValue;

class RandCall implements ICall {
    public function new(): Void {}

    public function call(args: Array<MerlinValue>): MerlinValue {
        if (args.length != 1)
            throw new ScriptError("rand() must have one and only one argument.");
        var val: Float = args[0].unwrapFloat();
        if (val < 0 || Math.floor(val) != val)
            throw new ScriptError("rand()'s argument must be a positive integer.");
        return new MerlinFloat(Std.random(Std.int(val)));
    }
}

class RandRef implements IRefFactory {
    public var name(default, null): String = Obfu.raw("rand");

    public function new(): Void {}

    public function createRef(game: GameMode): IRef {
        return new FrozenRef(MerlinFunction.fromCallable(new RandCall()));
    }
}

@:build(patchman.Build.di())
class Rand {
    @:diExport
    public var randRef(default, null): IRefFactory = new RandRef();

    public function new() {
    }
}
