package better_script.deadly_bottom;

import hf.mode.GameMode;
import merlin.value.MerlinValue;
import merlin.IRef;
import patchman.DebugConsole;

class DeadlyBottomRef implements IRef {
  public var game: GameMode;

  public function new(game: GameMode) {
    this.game = game;
  }

  public function get(): MerlinValue {
    return null;
  }

  public function set(value: MerlinValue): Bool {
    var b: Bool;
    if (value.isBool()) {
      b = value.unwrapBool();
    }
    else {
      var f: Float = value.unwrapFloat();
      if (f != 0 && f != 1)
        DebugConsole.log("Deprecated value for DEADLY_BOTTOM, please use false/true (or 0/1)");
      b = f > 0.5;
    }
    Reflect.setField(game, DeadlyBottom.SYMBOL.toString(), b);
    return true;
  }
}
