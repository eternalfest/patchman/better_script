package better_script.deadly_bottom;

import etwin.Obfu;
import hf.mode.GameMode;
import merlin.IRef;
import merlin.IRefFactory;

class DeadlyBottomRefFactory implements IRefFactory {
  public var name(default, null): String = Obfu.raw("DEADLY_BOTTOM");

  public function createRef(game: GameMode): IRef {
    return new DeadlyBottomRef(game);
  }
}
