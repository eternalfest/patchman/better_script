package better_script.macro;

#if macro
import haxe.macro.Expr;
import haxe.macro.Type;
import haxe.macro.Context;
import haxe.macro.ComplexTypeTools;
import etwin.Obfu;
import patchman.macro.BuildFields;

class MerlinProxy {

  public static inline var FIELD_META: String = ":merlinField";

  public static function build(): Array<Field> {
    var fields = Context.getBuildFields();
    var cls = Context.getLocalClass().get();

    var merlinFields = getReifiedMerlinFields(new BuildFields(fields));

    return fields.concat(getFieldsOf(cls.pos, macro: {

      private var __MERLIN_FIELDS(default, never): etwin.ds.FrozenMap<String, {
        var getField(default, null): Null<String>;
        var isGetter(default, null): Bool;
        var setField(default, null): Null<String>;
        var isSetter(default, null): Bool;
      }> = etwin.ds.FrozenMap.of($a{merlinFields});

      public function has(field: String): Bool {
        var index: Null<Int> = Std.parseInt(field);
        if (index != null) {
          field = Obfu.raw("__subscript");
        }
        return __MERLIN_FIELDS.exists(field);
      }

      public function get(field: String): etwin.ds.Nil<merlin.value.MerlinValue> {
        var index: Null<Int> = Std.parseInt(field);
        if (index != null) {
          // TODO: Find a better way for subscripting.
          field = Obfu.raw("__subscript");
          untyped this.subscript_index = index;
        }
        var info = __MERLIN_FIELDS.get(field);
        if (info == null || info.getField == null)
          return etwin.ds.Nil.none();
        if (info.isGetter) {
          return untyped this[info.getField]();
        } else {
          return untyped this[info.getField];
        }
      }

      public function set(field: String, value: merlin.value.MerlinValue): Void {
        var info = __MERLIN_FIELDS.get(field);
        if (info == null || info.setField == null)
          throw new merlin.ScriptError("Can't modify field " + field);
        if (info.isSetter) {
          untyped this[info.setField](value);
        } else {
          untyped this[info.setField] = value;
        }
      }

    }));
  }

  private static function getFieldsOf(pos: Position, ty: ComplexType): Array<Field> {
    return switch (ty) {
      case TAnonymous(fields):
        for (f in fields) f.pos = pos;
        fields;
      default: Context.error("type must be a struct", Context.currentPos());
    };
  }

  private static function getReifiedMerlinFields(fields: BuildFields): Array<Expr> {
    var merlinFields = [];
    var protectedFields: Map<String, Void> = new Map();

    function protectFromDCE(fieldName: Null<String>): Void {
      if (fieldName == null || protectedFields.exists(fieldName))
        return;

      protectedFields.set(fieldName, null);
      var field = fields.getByName(fieldName);
      if (field != null) {
        patchman.macro.MacroTools.addFieldMetadata(field, ":keep");
      }
    }

    for (f in fields.getByMeta(FIELD_META)) {
      var field = readMerlinField(f);

      var isGetter = field.get == AccCall;
      var getField = getMerlinFieldTarget(field, false);
      protectFromDCE(getField);

      var isSetter = field.set == AccCall;
      var setField = getMerlinFieldTarget(field, true);
      protectFromDCE(setField);

      merlinFields.push(macro ($e{field.merlinName}: String) => {
        getField: $v{getField},
        isGetter: $v{isGetter},
        setField: $v{setField},
        isSetter: $v{isSetter},
      });
    }

    return merlinFields;
  }

  private static function readMerlinField(f: FieldAndMeta): Null<MerlinField> {
    var pos = f.meta.pos;
    var merlinName = switch (f.meta.params) {
      case [expr]: expr;
      default: Context.error('MerlinProxy.build(): invalid @$FIELD_META', pos);
    };

    var isPublic = false;
    for (access in f.field.access) {
      switch (access) {
        case APublic: isPublic = true;
        case APrivate: isPublic = false;
        case AStatic: Context.error('MerlinProxy.build(): @$FIELD_META can\'t be used on static fields', pos);
        case AInline: Context.error('MerlinProxy.build(): @$FIELD_META can\'t be used on inline fields', pos);
        case AMacro: Context.error('MerlinProxy.build(): @$FIELD_META can\'t be used on macro fields', pos);
        case AOverride | ADynamic: null;
      }
    }

    if (!isPublic)
      Context.error('MerlinProxy.build(): @$FIELD_META can only be used on public fields or properties', pos);

    var getAccess: MerlinAccess = AccSimple;
    var setAccess: MerlinAccess = AccSimple;
    var fieldType = switch (f.field.kind) {
      case FVar(t, _): t;
      case FFun(_): Context.error('MerlinProxy.build(): @$FIELD_META can\'t be used on methods', pos);
      case FProp(get, set, t, _):
        getAccess = parseFieldAccess(get, pos);
        setAccess = parseFieldAccess(set, pos);
        t;
    };

    var fieldType = ComplexTypeTools.toType(fieldType);
    if (fieldType == null)
      Context.error('MerlinProxy.build(): missing type for @$FIELD_META field', pos);

    var merlinValueType = Context.getType("merlin.value.MerlinValue");
    if (getAccess != AccNone && !Context.unify(fieldType, merlinValueType))
      Context.error('MerlinProxy.build(): readable field must be a subtype of MerlinValue', pos);
    if (setAccess != AccNone && !Context.unify(merlinValueType, fieldType))
      Context.error('MerlinProxy.build(): writable field must be a supertype of MerlinValue', pos);

    return {
      merlinName: merlinName,
      realName: f.field.name,
      get: getAccess,
      set: setAccess,
    };
  }

  private static function getMerlinFieldTarget(field: MerlinField, asSetter: Bool): Null<String> {
    var access = asSetter ? field.set : field.get;
    return switch (access) {
      case AccSimple: field.realName;
      case AccNone: null;
      case AccCall: (asSetter ? "set_" : "get_") + field.realName;
    };
  }

  private static function parseFieldAccess(access: String, pos: Position): MerlinAccess {
    return switch (access) {
      case "default": AccSimple;
      case "never" | "null": AccNone;
      case "get" | "set": AccCall;
      default: Context.error('MerlinProxy.build(): unsupported access for field: $access', pos);
    };
  }
}

private typedef MerlinField = {
  merlinName: ExprOf<String>,
  realName: String,
  get: MerlinAccess,
  set: MerlinAccess,
};

private enum MerlinAccess {
  AccSimple;
  AccCall;
  AccNone;
}

#end
