package better_script.players;

import etwin.Obfu;
import hf.entity.Player;
import hf.mode.GameMode;
import merlin.IProxy;
import merlin.IRef;
import merlin.IRefFactory;
import merlin.ScriptError;
import merlin.refs.FrozenRef;
import merlin.value.MerlinFloat;
import merlin.value.MerlinObject;
import merlin.value.MerlinValue;

class PlayersRef implements IRefFactory {
    public var name(default, null): String = Obfu.raw("players");

    public function new(): Void {}

    public function createRef(game: GameMode): IRef {
        return new FrozenRef(MerlinObject.proxy(new PlayersProxy(game)));
    }
}

@:build(better_script.macro.MerlinProxy.build())
class PlayerProxy implements IProxy {
    private var player: Player;

    public function new(player: Player): Void {
        this.player = player;
    }

    @:merlinField(Obfu.raw("lives"))
    public var lives(get, set): MerlinValue;
    @:merlinField(Obfu.raw("x"))
    public var x(get, set): MerlinValue;
    @:merlinField(Obfu.raw("y"))
    public var y(get, set): MerlinValue;

    public function get_lives(): MerlinFloat {
        return player.lives;
    }

    public function set_lives(val: MerlinValue): MerlinFloat {
        var floatValue: MerlinFloat = val.unwrapFloat();
        var nbLives: Int = Std.int(Math.floor(floatValue));
        if (nbLives < 0)
            throw new ScriptError("players.lives cannot be negative.");

        player.lives = nbLives;
        player.game.gi.setLives(player.pid, player.lives);
        return nbLives;
    }

    public function get_x(): MerlinFloat {
        var mirroredCx: Int = player.game.root.Entity.x_rtc(player.x);
        /* From my reading of it, it seems that GameMode::flipCoordCase also work to un-flip it. */
        return player.game.flipCoordCase(mirroredCx);
    }

    public function set_x(val: MerlinValue): MerlinFloat {
        var cx: Float = val.unwrapFloat();
        var x: Float = player.game.root.Entity.x_ctr(player.game.flipCoordCase(cx));
        player.moveTo(x, player.y);
        return x;
    }

    public function get_y(): MerlinFloat {
        return player.game.root.Entity.y_rtc(player.y);
    }

    public function set_y(val: MerlinValue): MerlinFloat {
        var cy: Float = val.unwrapFloat();
        var y: Float = player.game.root.Entity.y_ctr(cy);
        player.moveTo(player.x, y);
        return y;
    }
}

@:build(better_script.macro.MerlinProxy.build())
class PlayersProxy implements IProxy {
    private var game: GameMode;

    @:merlinField(Obfu.raw("length"))
    public var length(get, never): MerlinValue;
    @:merlinField(Obfu.raw("__subscript"))
    public var subscript(get, never): MerlinValue;
    public var subscript_index: Int;
    @:merlinField(Obfu.raw("lives"))
    public var lives(get, set): MerlinValue;
    @:merlinField(Obfu.raw("score"))
    public var score(get, set): MerlinValue;
    @:merlinField(Obfu.raw("spawnX"))
    public var spawnX(get, set): MerlinValue;
    @:merlinField(Obfu.raw("spawnY"))
    public var spawnY(get, set): MerlinValue;
    @:merlinField(Obfu.raw("x"))
    public var x(never, set): MerlinValue;
    @:merlinField(Obfu.raw("y"))
    public var y(never, set): MerlinValue;

    public function get_length(): MerlinFloat {
        return game.getPlayerList().length;
    }

    public function get_subscript(): MerlinObject {
        return MerlinObject.proxy(new PlayerProxy(game.getPlayerList()[subscript_index]));
    }

    private static function getNbLives(players: Array<Player>): Int {
        var nbLivesTotal: Int = 0;
        for (player in players)
            nbLivesTotal += player.lives;
        return nbLivesTotal;
    }

    private static function truncateTowardZero(value: Float): Int {
        if (value >= 0)
            return Std.int(Math.floor(value));
        else
            return Std.int(Math.ceil(value));
    }

    /* Precondition: players.length > 0 */
    private static function getMinPlayerFor(players: Array<Player>, lessFunction: Player -> Player -> Bool): Player {
        var bestPlayer: Player = players[0];
        for (i in 1...players.length) {
            var player: Player = players[i];
            if (lessFunction(player, bestPlayer))
                bestPlayer = player;
        }
        return bestPlayer;
    }

    public function get_lives(): MerlinFloat {
        return getNbLives(game.getPlayerList());
    }

    public function set_lives(val: MerlinValue): MerlinFloat {
        var floatValue: MerlinFloat = val.unwrapFloat();
        var nbDesiredLivesTotal: Int = Std.int(Math.floor(floatValue));
        if (nbDesiredLivesTotal < 0)
            throw new ScriptError("players.lives cannot be negative.");

        var players: Array<Player> = game.getPlayerList();
        if (players.length == 0)
            return 0;
        var currentNbLivesTotal: Int = getNbLives(players);
        var diffLives = nbDesiredLivesTotal - currentNbLivesTotal;
        if (diffLives == 0)
            return nbDesiredLivesTotal;

        var nbRemainingDiffLives: Int = diffLives;
        var nbDiffLivesPerPlayer: Int = truncateTowardZero(diffLives / players.length);
        if (nbDiffLivesPerPlayer != 0) {
            for (player in players) {
                /* Don't remove more lives than what the player has (in case we are removing lives). */
                var nbDiffLivesForPlayer: Int = Std.int(Math.max(nbDiffLivesPerPlayer, -player.lives));
                player.lives += nbDiffLivesForPlayer;
                game.gi.setLives(player.pid, player.lives);
                nbRemainingDiffLives -= nbDiffLivesForPlayer;
            }
        }

        var livesComparisonFunction = nbRemainingDiffLives > 0 ? function(p1: Player, p2: Player) {return p1.lives < p2.lives;}
                                                               : function(p1: Player, p2: Player) {return p1.lives > p2.lives;}
        /* If we add lives, add them in priority to the players with the least amount, if we remove lives, remove them from the players with the most. */
        while (nbRemainingDiffLives != 0) {
            // TODO? Poor algorithm complexity (O(n^2)), but that souldn't be a problem in practice (few players and few remaining life changes to apply).
            var player: Player = getMinPlayerFor(players, livesComparisonFunction);
            if (nbRemainingDiffLives > 0) {
                ++player.lives;
                game.gi.setLives(player.pid, player.lives);
                --nbRemainingDiffLives;
            }
            else {
                --player.lives;
                game.gi.setLives(player.pid, player.lives);
                ++nbRemainingDiffLives;
            }
        }

        /* Remove potentially now superfluous '+', because the GameInterface.setLives doesn't properly handle bypassing the limit for it... */
        for (player in players) {
            if (player.lives <= game.root.gui.GameInterface.MAX_LIVES && game.gi.more[player.pid]._name != null)
                game.gi.more[player.pid].removeMovieClip();
        }
        return nbDesiredLivesTotal;
    }

    private static function getScore(players: Array<Player>): Int {
        var totalScore: Int = 0;
        for (player in players)
            totalScore += player.score;
        return totalScore;
    }

    public function get_score(): MerlinFloat {
        return getScore(game.getPlayerList());
    }

    /* Similar to set_lives, but we don't care giving a player a negative score. */
    public function set_score(val: MerlinValue): MerlinFloat {
        var floatValue: MerlinFloat = val.unwrapFloat();
        var nbDesiredScoreTotal: Int = Std.int(Math.floor(floatValue));

        var players: Array<Player> = game.getPlayerList();
        if (players.length == 0)
            return 0;

        var currentScoreTotal: Int = getScore(players);
        var diffScore = nbDesiredScoreTotal - currentScoreTotal;
        if (diffScore == 0)
            return nbDesiredScoreTotal;

        /* We defer the call to Player.getScore in order to not have just "1" or "-1" displayed due to the compensation loop after, but the real change. */
        var diffScores: Map<Int, Int> = new Map<Int, Int>();
        var nbDiffScorePerPlayer: Int = truncateTowardZero(diffScore / players.length);
        for (player in players)
            diffScores.set(player.pid, nbDiffScorePerPlayer);

        var nbRemainingDiffScore: Int = diffScore - nbDiffScorePerPlayer * players.length;
        var scoreComparisonFunction = nbRemainingDiffScore > 0 ? function(p1: Player, p2: Player) {return p1.score < p2.score;}
                                                               : function(p1: Player, p2: Player) {return p1.score > p2.score;}
        /* If we add points, add them in priority to the players with the least amount, if we remove points, remove them from the players with the most. */
        while (nbRemainingDiffScore != 0) {
            var player: Player = getMinPlayerFor(players, scoreComparisonFunction);
            if (nbRemainingDiffScore > 0) {
                diffScores.set(player.pid, diffScores.get(player.pid) + 1);
                --nbRemainingDiffScore;
            }
            else {
                diffScores.set(player.pid, diffScores.get(player.pid) - 1);
                ++nbRemainingDiffScore;
            }
        }

        for (player in players) {
            var diff: Int = diffScores.get(player.pid);
            if (diff != 0)
                player.getScore(player, player.specialMan.actives[95] ? Std.int(diff / 2) : diff);
        }

        return nbDesiredScoreTotal;
    }

    public function get_spawnX(): MerlinFloat {
        /* We want to unmirror it, as that's how the script will expect it. For example think of the script "players.x = players.spawnX". */

        var mirroredCx: Int = game.world.current.__dollar__playerX;
        /* From my reading of it, it seems that GameMode::flipCoordCase also work to un-flip it. */
        return game.flipCoordCase(mirroredCx);
    }

    public function set_spawnX(val: MerlinValue): MerlinFloat {
        var cx: Int = Std.int(game.flipCoordCase(Std.int(val.unwrapFloat())));
        game.world.current.__dollar__playerX = cx;
        return cx;
    }

    public function get_spawnY(): MerlinFloat {
        return game.world.current.__dollar__playerY;
    }

    public function set_spawnY(val: MerlinValue): MerlinFloat {
        var cy: Int = Std.int(val.unwrapFloat());
        game.world.current.__dollar__playerY = cy;
        return cy;
    }

    public function set_x(val: MerlinValue): MerlinFloat {
        var cx: Float = val.unwrapFloat();
        var x: Float = game.root.Entity.x_ctr(game.flipCoordCase(cx));
        for (player in game.getPlayerList())
            player.moveTo(x, player.y);
        return x;
    }

    public function set_y(val: MerlinValue): MerlinFloat {
        var cy: Float = val.unwrapFloat();
        var y: Float = game.root.Entity.y_ctr(cy);
        for (player in game.getPlayerList())
            player.moveTo(player.x, y);
        return y;
    }

    public function new(game: GameMode): Void {
        this.game = game;
    }
}
