package better_script;

import etwin.Obfu;
import etwin.flash.MovieClip;
import hf.Entity;
import hf.levels.ScriptEngine;
import hf.mode.GameMode;
import merlin.IProxy;
import merlin.IRef;
import merlin.IRefFactory;
import merlin.ScriptError;
import merlin.refs.FrozenRef;
import merlin.value.MerlinFloat;
import merlin.value.MerlinObject;
import merlin.value.MerlinValue;

class SpritesRef implements IRefFactory {
    public var name(default, null): String = Obfu.raw("sprites");

    public function new(): Void {}

    public function createRef(game: GameMode): IRef {
        return new FrozenRef(MerlinObject.proxy(new SpritesProxy(game)));
    }
}

@:build(better_script.macro.MerlinProxy.build())
class SpriteProxy implements IProxy {
    private var game: GameMode;
    private var entityOrMovieClip: MovieClip;

    public function new(game: GameMode, entityOrMovieClip: MovieClip): Void {
        this.game = game;
        this.entityOrMovieClip = entityOrMovieClip;
    }

    @:merlinField(Obfu.raw("x"))
    public var x(get, set): MerlinValue;
    @:merlinField(Obfu.raw("y"))
    public var y(get, set): MerlinValue;

    private function getX(): Float {
        if (Std.is(entityOrMovieClip, game.root.Entity)) {
            var entity: Entity = cast entityOrMovieClip;
            return entity.x;
        }
        else {
            return entityOrMovieClip._x;
        }
    }

    private function setX(x: Float): Void {
        if (Std.is(entityOrMovieClip, game.root.Entity)) {
            var entity: Entity = cast entityOrMovieClip;
            entity.moveTo(x, entity.y);
        }
        else {
            entityOrMovieClip._x = x;
        }
    }

    private function getY(): Float {
        if (Std.is(entityOrMovieClip, game.root.Entity)) {
            var entity: Entity = cast entityOrMovieClip;
            return entity.y;
        }
        else {
            return entityOrMovieClip._y;
        }
    }

    private function setY(y: Float): Void {
        if (Std.is(entityOrMovieClip, game.root.Entity)) {
            var entity: Entity = cast entityOrMovieClip;
            entity.moveTo(entity.x, y);
        }
        else {
            entityOrMovieClip._y = y;
        }
    }

    public function get_x(): MerlinFloat {
        var mirroredCx: Int = game.root.Entity.x_rtc(getX());
        return game.flipCoordCase(mirroredCx);
    }

    public function set_x(val: MerlinValue): MerlinFloat {
        var cx: Float = val.unwrapFloat();
        var x: Float = game.root.Entity.x_ctr(game.flipCoordCase(cx));
        setX(x);
        return x;
    }

    public function get_y(): MerlinFloat {
        return game.root.Entity.y_rtc(getY());
    }

    public function set_y(val: MerlinValue): MerlinFloat {
        var cy: Float = val.unwrapFloat();
        var y: Float = game.root.Entity.y_ctr(cy);
        setY(y);
        return y;
    }
}

@:build(better_script.macro.MerlinProxy.build())
class SpritesProxy implements IProxy {
    private var game: GameMode;

    public function new(game: GameMode): Void {
        this.game = game;
    }

    @:merlinField(Obfu.raw("__subscript"))
    public var subscript(get, never): MerlinValue;
    public var subscript_index: Int;

    public function get_subscript(): MerlinObject {
        /* Similar to how hf/levels/ScriptEngine::killById finds the mc. */
        for (bad in game.getList(game.root.Data.ENTITY)) {
            if (bad.scriptId == subscript_index)
                return MerlinObject.proxy(new SpriteProxy(game, bad));
        }
        for (smc in game.world.scriptEngine.mcList) {
            if (smc.sid == subscript_index)
                return MerlinObject.proxy(new SpriteProxy(game, smc.mc));
        }
        throw new ScriptError("Could not find sprites[" + subscript_index + "].");
    }
}

@:build(patchman.Build.di())
class Sprites {
    @:diExport
    public var spritesRef(default, null): IRefFactory = new SpritesRef();

    public function new() {
    }
}
