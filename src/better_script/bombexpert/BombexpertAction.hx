package better_script.bombexpert;

import hf.mode.GameMode;
import merlin.IAction;
import merlin.IActionContext;

class BombexpertAction implements IAction {
  public var name(default, null): String;
  public var id: Int;
  public var isVerbose(default, null): Bool = false;

  public function new(name: String) {
    this.name = name;
  }

  public function run(ctx: IActionContext): Bool {
    var game: GameMode = ctx.getGame();
    return game.root.GameManager.CONFIG.hasOption(game.root.Data.OPT_BOMB_EXPERT);
  }
}
