package better_script;

import etwin.ds.FrozenArray;
import merlin.IAction;
import merlin.IRefFactory;
import patchman.IPatch;
import patchman.Patchman;

@:build(patchman.Build.di())
class BetterScript {

  @:diExport
  public var actions(default, null): FrozenArray<IAction>;

  @:diExport
  public var patches(default, null): FrozenArray<IPatch>;

  @:diExport
  public var refFactories(default, null): FrozenArray<IRefFactory>;

  public function new(
    bombexpert: Bombexpert,
    deadlyBottom: DeadlyBottom,
    debug: Debug,
    noBomb: NoBomb,
    noNextLevel: NoNextLevel,
    players: Players,
    rand: Rand,
    sprite: Sprite,
    sprites: Sprites,
    actions: Array<IAction>,
    patches: Array<IPatch>,
    refFactories: Array<IRefFactory>
  ): Void {
    this.actions = FrozenArray.from(actions);
    this.patches = FrozenArray.from(patches);
    this.refFactories = FrozenArray.from(refFactories);
  }

}
