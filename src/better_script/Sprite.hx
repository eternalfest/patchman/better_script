package better_script;

import better_script.sprite.SpriteAction;
import better_script.sprite.SpriteFilterProxy;
import etwin.Obfu;
import merlin.IAction;
import merlin.IRefFactory;
import merlin.refs.FrozenFactory;
import merlin.value.MerlinFunction;

@:build(patchman.Build.di())
class Sprite {

  public static var FILTER_HSV(default, null): IRefFactory = new FrozenFactory(Obfu.raw("filter_hsv"),
    MerlinFunction.fromFunc(SpriteFilterProxy.filter_hsv));

  @:diExport
  public var spriteAction(default, never): IAction = new SpriteAction();

  @:diExport
  public var filterHsvRef(default, never): IRefFactory = FILTER_HSV;

  public function new() {}

}