package better_script;

import better_script.bombexpert.BombexpertAction;
import etwin.Obfu;
import merlin.IAction;

@:build(patchman.Build.di())
class Bombexpert {
  @:diExport
  public var action(default, null): IAction = new BombexpertAction(Obfu.raw("bombexpert"));

  public function new() {
  }
}
