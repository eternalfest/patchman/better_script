package better_script.sprite;

import etwin.Obfu;
import etwin.ds.Nil;
import etwin.flash.MovieClip;
import etwin.flash.filters.BitmapFilter;
import hf.Hf;
import hf.mode.GameMode;
import merlin.IAction;
import merlin.IActionContext;
import merlin.value.MerlinValue;
import merlin.value.MerlinObject;
import patchman.DebugConsole;

class SpriteAction implements IAction {

  public var name(default, null): String = Obfu.raw("sprite");
  public var isVerbose(default, null): Bool = false;

  public function new(): Void {}

  public function run(ctx: IActionContext): Bool {
    var hf: Hf = ctx.getHf();
    var game: GameMode = ctx.getGame();

    var x: Nil<Float> = ctx.getOptFloat(Obfu.raw("x"));
    var y: Nil<Float> = ctx.getOptFloat(Obfu.raw("y"));
    var xr: Nil<Float> = ctx.getOptFloat(Obfu.raw("xr"));
    var yr: Nil<Float> = ctx.getOptFloat(Obfu.raw("yr"));
    var sid: Null<Int> = ctx.getOptInt(Obfu.raw("sid")).toNullable();
    var back: Bool = ctx.getOptBool(Obfu.raw("back")).or(false);
    var n: String = ctx.getString(Obfu.raw("n"));
    var p: Bool = ctx.getOptBool(Obfu.raw("p")).or(false);
    var flip: Bool = ctx.getOptBool(Obfu.raw("flip")).or(false);
    var sx: Nil<Float> = ctx.getOptFloat(Obfu.raw("sx"));
    var sy: Nil<Float> = ctx.getOptFloat(Obfu.raw("sy"));
    var s: Nil<Float> = ctx.getOptFloat(Obfu.raw("s"));
    var rot: Nil<Float> = ctx.getOptFloat(Obfu.raw("rot"));
    var frame: Nil<Int> = ctx.getOptInt(Obfu.raw("frame"));
    var sframe: Nil<Int> = ctx.getOptInt(Obfu.raw("sframe"));
    var filters: Nil<MerlinValue> = ctx.getOptArg(Obfu.raw("filters"));
    var alpha: Nil<Float> = ctx.getOptFloat(Obfu.raw("alpha"));

    // calculate xScale and yScale using s, sx, sy, flip and fl_mirror
    var xScale: Float = 1;
    var yScale: Float = 1;
    sx.map(scale => xScale = scale);
    sy.map(scale => yScale = scale);
    s.map(scale => {
      if (sx.isSome()) {
        DebugConsole.warn("ConflictingSpriteParameters: s and sx cannot be used at the same time");
      }
      if (sy.isSome()) {
        DebugConsole.warn("ConflictingSpriteParameters: s and sy cannot be used at the same time");
      }
      xScale = scale;
      yScale = scale;
    });
    if (game.fl_mirror != flip) {
      xScale = -xScale;
    }

    // sid
    ctx.killById(sid);
    // x / xr
    var rx: Float = if (x.isSome()) {
      if (xr.isSome()) {
        DebugConsole.warn("ConflictingSpriteParameters: x and xr cannot be used at the same time");
      }
      hf.Entity.x_ctr(x.unwrap());
    } else if (xr.isSome()) {
      xr.unwrap();
    } else {
      DebugConsole.warn("MissingSpriteCoordinates: Missing x / xr coordinates for sprite " + n);
      0;
    }
    // y / yr
    var ry: Float = if (y.isSome()) {
      if (yr.isSome()) {
        DebugConsole.warn("ConflictingSpriteParameters: y and yr cannot be used at the same time");
      }
      hf.Entity.y_ctr(y.unwrap());
    } else if (yr.isSome()) {
      yr.unwrap();
    } else {
      DebugConsole.warn("MissingSpriteCoordinates: Missing y / yr coordinates for sprite " + n);
      0;
    }
    rx = game.flipCoordReal(rx);
    // adjust x position depending on xScale
    rx += (1 - xScale) * 0.5 * hf.Data.CASE_WIDTH;
    // n / back
    var sprite: Null<MovieClip> = game.world.view.attachSprite(n, rx, ry, back);
    if (sprite == null) {
      DebugConsole.warn("AttachSpriteFailed: Could not attach sprite " + n);
      return false;
    }
    // apply xScale and yScale
    sprite._xscale = xScale * 100;
    sprite._yscale = yScale * 100;
    // rot
    rot.map(r => sprite._rotation = r);
    if (game.fl_mirror) {
      sprite._rotation = -sprite._rotation;
    }
    // frame
    frame.map(f => sprite.gotoAndStop(f));
    // subframe
    untyped { sprite.sub.gotoAndStop(sframe); }
    // p
    if (p) {
      sprite.play();
    } else {
      sprite.stop();
    }
    if (Reflect.hasField(sprite, "sub")) {
      Reflect.getProperty(sprite, "sub").stop();
    }
    // filters
    filters.map(filter => {
      var filterObject: MerlinObject = filter.unwrapObject();
      if (Std.is(filterObject, SpriteFilterProxy)) {
        var bitmapFilter: BitmapFilter = (cast filterObject: SpriteFilterProxy).getFilter();
        sprite.filters = [bitmapFilter];
      } else {
        DebugConsole.warn("InvalidSpriteFilters: Invalid sprite filters for sprite " + n);
      }
    });
    // alpha
    alpha.map(a => sprite._alpha = a);
    // $torch holes
    if (n == "$torch") {
      if (!ctx.getFirstTorch()) {
        game.clearExtraHoles();
      }
      game.addHole(rx + xScale * hf.Data.CASE_WIDTH * 0.5, ry - hf.Data.CASE_HEIGHT * 0.5, 180);
      game.updateDarkness();
      ctx.setFirstTorch(true);
    }
    ctx.registerMc(sid, sprite);
    return false;
  }

}