package better_script.sprite;

import color_matrix.ColorMatrix;
import etwin.ds.Nil;
import etwin.flash.filters.BitmapFilter;
import merlin.IProxy;
import merlin.ScriptError;
import merlin.value.MerlinObject;
import merlin.value.MerlinValue;

class SpriteFilterProxy implements IProxy {

  var filter: BitmapFilter;

  public function new(filter: BitmapFilter): Void {
    this.filter = filter;
  }

  public function getFilter(): BitmapFilter {
    return this.filter;
  }

  public function has(key: String): Bool {
    return false;
  }
  
  public function get(key: String): Nil<MerlinValue> {
    return Nil.none();
  }

  public function set(key: String, value: MerlinValue): Void {
    throw new merlin.ScriptError("Can't modify field " + key + " on sprite filter");
  }

  public static function filter_hsv(args: Array<MerlinValue>): MerlinValue {
    if (args.length != 3) {
      throw new ScriptError("filter_hsv() must have exactly 3 arguments.");
    }
    var h: Float = args[0].unwrapFloat();
    var s: Float = args[1].unwrapFloat();
    var v: Float = args[2].unwrapFloat();
    var matrix: ColorMatrix = ColorMatrix.fromHsv(h, s, v);
    return MerlinObject.proxy(new SpriteFilterProxy(matrix.toFilter()));
  }

}