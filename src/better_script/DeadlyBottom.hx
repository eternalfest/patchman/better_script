package better_script;

import better_script.deadly_bottom.DeadlyBottomRefFactory;
import etwin.ds.Nil;
import etwin.Symbol;
import hf.levels.GameMechanics;
import hf.entity.Player;
import merlin.IRefFactory;
import patchman.Ref;
import patchman.IPatch;

@:build(patchman.Build.di())
class DeadlyBottom {
  public static var SYMBOL(default, never): Symbol = new Symbol();

  @:diExport
  public var applyDeadlyBottom(default, null): IPatch;
  @:diExport
  public var resetDeadlyBottom(default, null): IPatch;
  @:diExport
  public var merlinRef(default, null): IRefFactory = new DeadlyBottomRefFactory();

  public function new() {
    applyDeadlyBottom = Ref.auto(Player.onDeathLine).prefix(function(hf, self: Player): Nil<Void> {
      if (Reflect.field(self.game, SYMBOL.toString()) && !self.fl_kill) {
        self.y = hf.Data.LEVEL_HEIGHT * hf.Data.CASE_HEIGHT - hf.Data.CASE_HEIGHT - 1;
        self.dx = 0;
        self.forceKill(0);
        return Nil.some(null);
      }
      return Nil.none();
    });

    resetDeadlyBottom = Ref.auto(GameMechanics.onReadComplete).before(function(hf, self: GameMechanics) {
      Reflect.setField(self.game, SYMBOL.toString(), false);
    });
  }
}
