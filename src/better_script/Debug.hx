package better_script;

import etwin.Obfu;
import hf.mode.GameMode;
import merlin.ICall;
import merlin.IRef;
import merlin.IRefFactory;
import merlin.refs.FrozenRef;
import merlin.value.MerlinFloat;
import merlin.value.MerlinFunction;
import merlin.value.MerlinString;
import merlin.value.MerlinValue;
import patchman.DebugConsole;

class DebugCall implements ICall {
    public function new(): Void {}

    private function stringify(val: MerlinValue): String {
        if (val.isBool())
            return Std.string(val.unwrapBool());
        else if (val.isFloat())
            return Std.string(val.unwrapFloat());
        else if (val.isNull())
            return "null";
        else if (val.isString())
            return val.unwrapString();
        else if (val.isObject())
            return "<object>";
        else if (val.isFunction())
            return "<function>";
        return "<unhandled-merlin-value>";
    }

    public function call(args: Array<MerlinValue>): MerlinValue {
        var str: String = "";
        for (arg in args)
            str = str + stringify(arg);
        DebugConsole.log(str);
        return new MerlinString(str);
    }
}

class DebugRef implements IRefFactory {
    public var name(default, null): String = Obfu.raw("debug");

    public function new(): Void {}

    public function createRef(game: GameMode): IRef {
        return new FrozenRef(MerlinFunction.fromCallable(new DebugCall()));
    }
}

/* Dirty hackish workaround the fact that we cannot "just" call a Ref of ICall, we need to try to affect it to something...
 * So use debug in script like so:
 *     DUMP = debug("position: (", players[0].x, ", ", players[0].y, "), lives=", players[0].lives);
 */
class DumpRef implements IRef {
    public function new(): Void {
    }

    public function get(): MerlinValue {
        return null;
    }

    public function set(value: MerlinValue): Bool {
        return true;
    }
}
class DumpRefFactory implements IRefFactory {
    public var name(default, null): String = Obfu.raw("DUMP");

    public function createRef(game: GameMode): IRef {
        return new DumpRef();
    }
}


@:build(patchman.Build.di())
class Debug {
    @:diExport
    public var debugRef(default, null): IRefFactory = new DebugRef();
    @:diExport
    public var dumpRef(default, null): IRefFactory = new DumpRefFactory();

    public function new() {
    }
}
