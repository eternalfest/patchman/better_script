package better_script;

import better_script.players.PlayersRef;
import merlin.IRefFactory;

@:build(patchman.Build.di())
class Players {
    @:diExport
    public var playersRef(default, null): IRefFactory = new PlayersRef();

    public function new() {
    }
}
