package better_script.no_next_level;

import etwin.Obfu;
import hf.mode.GameMode;
import merlin.IRef;
import merlin.IRefFactory;

class NoNextLevelRefFactory implements IRefFactory {
  public var name(default, null): String = Obfu.raw("NO_NEXT_LEVEL");

  public function createRef(game: GameMode): IRef {
    return new NoNextLevelRef(game);
  }
}
