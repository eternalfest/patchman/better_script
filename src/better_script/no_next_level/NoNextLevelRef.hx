package better_script.no_next_level;

import better_script.NoNextLevel;
import hf.mode.GameMode;
import merlin.IRef;
import merlin.value.MerlinValue;
import patchman.DebugConsole;

class NoNextLevelRef implements IRef {
  /**
   * Returns the sub-proxy corresponding to the provided field.
   */
  public var game: GameMode;

  public function new(game: GameMode) {
    this.game = game;
  }

  public function get(): MerlinValue {
    return null;
  }

  /**
   * Result: Boolean indicating if the value was updated.
   * (`false` indicates an error such as readonly or invalid fields)
   */
  public function set(value: MerlinValue): Bool {
    var b: Bool;
    if (value.isBool()) {
      b = value.unwrapBool();
    }
    else {
      var f: Float = value.unwrapFloat();
      if (f != 0 && f != 1)
        DebugConsole.log("Deprecated value for NO_NEXT_LEVEL, please use false/true (or 0/1)");
      b = f > 0.5;
    }
    Reflect.setField(game, NoNextLevel.SYMBOL.toString(), b);
    game.fxMan.attachExit();
    return true;
  }
}
