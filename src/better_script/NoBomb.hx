package better_script;

import etwin.ds.Nil;
import etwin.Obfu;
import etwin.Symbol;
import hf.entity.Player;
import hf.levels.GameMechanics;
import hf.mode.GameMode;
import patchman.Ref;
import patchman.IPatch;
import merlin.IRef;
import merlin.IRefFactory;
import merlin.value.MerlinValue;

class NoBombRef implements IRef {
    public var game: GameMode;

    public function new(game: GameMode): Void {
        this.game = game;
    }

    public function get(): MerlinValue {
        return null;
    }

    public function set(value: MerlinValue): Bool {
        Reflect.setField(game, NoBomb.SYMBOL.toString(), value.unwrapBool());
        return true;
    }
}

class NoBombRefFactory implements IRefFactory {
    public var name(default, null): String = Obfu.raw("NO_BOMB");

    public function createRef(game: GameMode): IRef {
        return new NoBombRef(game);
    }
}

@:build(patchman.Build.di())
class NoBomb {
    public static var SYMBOL(default, never): Symbol = new Symbol();

    @:diExport
    public var applyNoBomb(default, null): IPatch;
    @:diExport
    public var resetNoBomb(default, null): IPatch;
    @:diExport
    public var nbBombRef(default, null): IRefFactory = new NoBombRefFactory();

    public function new(): Void {
        applyNoBomb = Ref.auto(Player.attack).prefix(function(hf, self: Player): Nil<Null<hf.Entity>> {
            if (Reflect.field(self.game, SYMBOL.toString()))
                return Nil.some(null);
            return Nil.none();
        });

        resetNoBomb = Ref.auto(GameMechanics.onReadComplete).before(function(hf, self: GameMechanics) {
            Reflect.setField(self.game, SYMBOL.toString(), false);
        });
    }
}
